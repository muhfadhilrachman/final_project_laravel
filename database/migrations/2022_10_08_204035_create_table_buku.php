<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableBuku extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('table_buku', function (Blueprint $table) {
            $table->id();
            $table->string('nama');

            $table->unsignedBigInteger('penerbit_name');
           $table->foreign('penerbit_name')->references('id')->on('table_penerbit');

           
           $table->unsignedBigInteger('genre_name');
           $table->foreign('genre_name')->references('id')->on('table_genre');

            $table->text('deskripsi');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_buku');
    }
}

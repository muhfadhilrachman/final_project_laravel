<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class penerbitController extends Controller
{
    //
    public function get()
    {
        // return view('listBuku');
        $data = DB::table('table_penerbit')->get();
        return view('penerbit.listPenerbit',['data'=>$data]);

    }

    public function create(Request $request)
    {

        $request->validate([
            'nama'=> 'required',
            'deskripsi'=> 'required',
            'alamat'=> 'required',
        ]);
        DB::table('table_penerbit')->insert(
            [
                'nama'=>$request['nama'],
                'deskripsi'=>$request['deskripsi'],
                'alamat'=>$request['alamat'],
            ]
            );
        return redirect('/penerbit');
    }

    public function update($id, Request $request)
    {

        $request->validate([
            'nama'=> 'required',
            'deskripsi'=> 'required',
            'alamat'=> 'required',
        ]);
        
        DB::table('table_penerbit')
        ->where('id',$id)
        ->update(
            [
                'nama'=>$request['nama'],
                'deskripsi'=>$request['deskripsi'],
                'alamat'=>$request['alamat'],
            ]
            );
        return redirect('/penerbit');

    }

    public function delete($id)
    {
        DB::table('table_penerbit')
        ->where('id',$id)
        ->delete();
        
        return redirect('/penerbit');
        
    }
}
